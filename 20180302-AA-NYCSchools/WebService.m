//
//  WebService.m
//  20180302-AA-NYCSchools
//
//  Created by Argueta, Adan (CHICO-C) on 3/2/18.
//  Copyright © 2018 Argueta, Adan (CHICO-C). All rights reserved.
//

#import "WebService.h"
#import "AFNetworking.h"
#import "Mantle.h"
#import "School.h"
#import "SAT.h"

@interface WebService ()
@property (nonatomic, strong) AFHTTPSessionManager *manager;
@end

@implementation WebService

- (AFHTTPSessionManager *)manager {
    static AFHTTPSessionManager *manager = nil;
    static dispatch_once_t onceToken;
    //Singleton instance of the manager
    dispatch_once(&onceToken, ^{
        manager = [self managerWithRequestSerializer:[AFJSONRequestSerializer serializer] andResponseSerializer:[AFJSONResponseSerializer new]];
        manager.session.configuration.URLCache = nil;
    });
    return manager;
}

- (AFHTTPSessionManager *)managerWithRequestSerializer:(AFHTTPRequestSerializer *)requestSerializer andResponseSerializer:(AFHTTPResponseSerializer *)responseSerializer {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];    
    manager.requestSerializer = requestSerializer;
    manager.responseSerializer = responseSerializer;
    
    return manager;
}

- (void)downloadSchoolsDataWithCompletionBlock:(void (^)(NSArray *schools, NSError *error))completionBlock {
    [self.manager GET:@"https://data.cityofnewyork.us/resource/97mf-9njv.json" parameters:nil success:^(NSURLSessionDataTask *task, NSArray *schools) {
        NSArray *convertedSchools = [self convertArray:schools toModelClass:[School class]];

        completionBlock(convertedSchools, nil);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        completionBlock(nil, error);
    }];
}

- (void)downloadSATsDataWithCompletionBlock:(void (^)(NSArray *SATs, NSError *error))completionBlock {
    [self.manager GET:@"https://data.cityofnewyork.us/resource/734v-jeq5.json" parameters:nil success:^(NSURLSessionDataTask *task, NSArray *SATs) {
        NSArray *convertedSATs = [self convertArray:SATs toModelClass:[SAT class]];
        completionBlock(convertedSATs, nil);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        completionBlock(nil, error);
    }];
}

//convenient method to convert a webservice call response to an array of objects
- (NSArray *)convertArray:(NSArray *)array toModelClass:(Class)class {
    if (!array) {
        return @[];
    } else if (![array isKindOfClass:[NSArray class]]) {
        return @[];
    }
    
    NSError *error = nil;
    NSArray *convertedObjects = [MTLJSONAdapter modelsOfClass:class fromJSONArray:array error:&error];
    
    if(error) {
        return nil;
    } else {
        return convertedObjects;
    }
}

@end
