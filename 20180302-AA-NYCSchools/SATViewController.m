//
//  SATViewController.m
//  20180302-AA-NYCSchools
//
//  Created by Argueta, Adan (CHICO-C) on 3/2/18.
//  Copyright © 2018 Argueta, Adan (CHICO-C). All rights reserved.
//

#import "SATViewController.h"

@interface SATViewController ()

@property (nonatomic, strong)SAT *schoolSAT;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *readingtViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mathViewHeight;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIView *graphsContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *writingViewHeight;
@property (weak, nonatomic) IBOutlet UILabel *readingScoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *writingScoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *mathScoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *averageScoreLabel;

@end

@implementation SATViewController

- (instancetype)initWithSAT:(SAT*)schoolSAT {
    UIStoryboard*  sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    self = [sb instantiateViewControllerWithIdentifier:@"SATViewController"];
    if (self) {
        _schoolSAT = schoolSAT;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self configureUI];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self animate];
}

- (void)configureUI {
    self.name.text = self.schoolSAT.name;
    self.averageScoreLabel.text = [NSString stringWithFormat:@"Average STA's Scores from %@ students", self.schoolSAT.takersCount];
    self.mathScoreLabel.text = self.schoolSAT.mathScore;
    self.writingScoreLabel.text = self.schoolSAT.writingScore;
    self.readingScoreLabel.text = self.schoolSAT.readingScore;
    
    self.mathScoreLabel.alpha = 0;
    self.writingScoreLabel.alpha = 0;
    self.readingScoreLabel.alpha = 0;
    
    self.writingViewHeight.constant = 0;
    self.mathViewHeight.constant = 0;
    self.readingtViewHeight.constant = 0;
}

- (void)animate {
    int maxHeight = self.graphsContainer.frame.size.height-50;
    long mathHeight = ([self.schoolSAT.mathScore integerValue]*maxHeight) / 800;
    long readingtHeight = ([self.schoolSAT.readingScore integerValue]*maxHeight) / 800;
    long writingtHeight = ([self.schoolSAT.writingScore integerValue]*maxHeight) / 800;
    
    
    [UIView animateWithDuration:2 animations:^{
        self.mathViewHeight.constant = mathHeight;
        self.readingtViewHeight.constant = readingtHeight;
        self.writingViewHeight.constant = writingtHeight;
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        
        self.mathScoreLabel.alpha = 1;
        self.writingScoreLabel.alpha = 1;
        self.readingScoreLabel.alpha = 1;
    }];
}

- (IBAction)closeButtonTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
