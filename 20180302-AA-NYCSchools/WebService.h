//
//  WebService.h
//  20180302-AA-NYCSchools
//
//  Created by Argueta, Adan (CHICO-C) on 3/2/18.
//  Copyright © 2018 Argueta, Adan (CHICO-C). All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WebService : NSObject

- (void)downloadSchoolsDataWithCompletionBlock:(void (^)(NSArray *schools, NSError *error))completionBlock;
- (void)downloadSATsDataWithCompletionBlock:(void (^)(NSArray *schools, NSError *error))completionBlock;

@end
