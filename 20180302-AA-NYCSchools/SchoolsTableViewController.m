//
//  SchoolsTableViewController.m
//  20180302-AA-NYCSchools
//
//  Created by Argueta, Adan (CHICO-C) on 3/2/18.
//  Copyright © 2018 Argueta, Adan (CHICO-C). All rights reserved.
//

#import "SchoolsTableViewController.h"
#import "WebService.h"
#import "SVProgressHUD.h"
#import "School.h"
#import "SchoolDetailsViewController.h"

@interface SchoolsTableViewController () <UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong)NSArray *schools;
@end

@implementation SchoolsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.clearsSelectionOnViewWillAppear = YES;
    //pull down the table to refresh the school data
    [self.tableView.refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventAllEvents];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //Validation to not refresh data when coming back from the school details screen
    if (!self.schools) {
        [self fetchSchoolsData];
    }
}

- (void)fetchSchoolsData{
    [SVProgressHUD show];
    
    [[[WebService alloc]init] downloadSchoolsDataWithCompletionBlock:^(NSArray *schools, NSError *error) {
        [SVProgressHUD dismiss];
        if (error) {
            [self showErrorAlert];
            return;
        }
        self.schools = schools;
        [self.tableView reloadData];
    }];
}

- (void)showErrorAlert {
    UIAlertController *errorAlert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Could not fetch NYC Schools Data" preferredStyle:UIAlertControllerStyleAlert];
    [errorAlert addAction:[UIAlertAction actionWithTitle:@"Try again" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self fetchSchoolsData];
    }]];
    //if there's no schools data the only option is to try again because we wont want to show an empty table
    if (self.schools) {
        [errorAlert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    }
    [self presentViewController:errorAlert animated:YES completion:nil];

}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.schools.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    School *school = self.schools[indexPath.row];
    cell.textLabel.text = school.name;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    SchoolDetailsViewController *schoolDetails = [[SchoolDetailsViewController alloc]initWithSchool:self.schools[indexPath.row]];
    [self.navigationController pushViewController:schoolDetails animated:YES];
}

- (void)refresh:(UIRefreshControl *)refreshControl {
    [self fetchSchoolsData];
    [refreshControl endRefreshing];
}

@end
