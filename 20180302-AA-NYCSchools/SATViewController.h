//
//  SATViewController.h
//  20180302-AA-NYCSchools
//
//  Created by Argueta, Adan (CHICO-C) on 3/2/18.
//  Copyright © 2018 Argueta, Adan (CHICO-C). All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SAT.h"

@interface SATViewController : UIViewController

- (instancetype)initWithSAT:(SAT*)schoolSAT;

@end
