//
//  SAT.h
//  20180302-AA-NYCSchools
//
//  Created by Argueta, Adan (CHICO-C) on 3/2/18.
//  Copyright © 2018 Argueta, Adan (CHICO-C). All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Mantle.h"

@interface SAT  : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong) NSString *dbn;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *takersCount;
@property (nonatomic, strong) NSString *mathScore;
@property (nonatomic, strong) NSString *readingScore;
@property (nonatomic, strong) NSString *writingScore;

@end
