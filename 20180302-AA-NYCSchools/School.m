//
//  School.m
//  20180302-AA-NYCSchools
//
//  Created by Argueta, Adan (CHICO-C) on 3/2/18.
//  Copyright © 2018 Argueta, Adan (CHICO-C). All rights reserved.
//

#import "School.h"

@implementation School

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             NSStringFromSelector(@selector(dbn)) : @"dbn",
             NSStringFromSelector(@selector(name)) : @"school_name",
             NSStringFromSelector(@selector(address)) : @"primary_address_line_1",
             NSStringFromSelector(@selector(email)) : @"school_email",
             NSStringFromSelector(@selector(overview)) : @"overview_paragraph"
             };
    
}
@end
