//
//  SchoolDetailsViewController.m
//  20180302-AA-NYCSchools
//
//  Created by Argueta, Adan (CHICO-C) on 3/2/18.
//  Copyright © 2018 Argueta, Adan (CHICO-C). All rights reserved.
//

#import "SchoolDetailsViewController.h"
#import "SATViewController.h"
#import "WebService.h"
#import "SVProgressHUD.h"

@interface SchoolDetailsViewController ()

@property (nonatomic, strong)School *school
;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *address;
@property (weak, nonatomic) IBOutlet UILabel *email;
@property (weak, nonatomic) IBOutlet UILabel *overview;

@end

@implementation SchoolDetailsViewController

- (instancetype)initWithSchool:(School *)school{
    UIStoryboard*  sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    self = [sb instantiateViewControllerWithIdentifier:@"details"];
    if (self) {
        _school = school;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.name.text = self.school.name;
    self.address.text = self.school.address;
    self.email.text = self.school.email;
    self.overview.text = self.school.overview;
}

- (IBAction)SATsButtonTapped:(id)sender {
    [SVProgressHUD show];
    [[[WebService alloc]init] downloadSATsDataWithCompletionBlock:^(NSArray *SATs, NSError *error) {
        [SVProgressHUD dismiss];
        if (error) {
            [self showErrorAlert];
            return;
        }
        SAT *schoolSAT = [self SATForDBN:self.school.dbn fromSATsArray:SATs];
        if (schoolSAT) {
            [self presentViewController:[[SATViewController alloc]initWithSAT:schoolSAT] animated:YES completion:nil];
        } else {
            UIAlertController *errorAlert = [UIAlertController alertControllerWithTitle:@"No results" message:@"There are not SAT scores for this school at this time" preferredStyle:UIAlertControllerStyleAlert];
            [errorAlert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            }]];
            [self presentViewController:errorAlert animated:YES completion:nil];

        }
    }];
}

- (void)showErrorAlert {
    UIAlertController *errorAlert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Could not fetch SAT's Data" preferredStyle:UIAlertControllerStyleAlert];
    [errorAlert addAction:[UIAlertAction actionWithTitle:@"Try again" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self SATsButtonTapped:self];
    }]];
    
    [self presentViewController:errorAlert animated:YES completion:nil];

}

- (SAT *)SATForDBN:(NSString*)DBN fromSATsArray:(NSArray *)SATsArray {
    NSPredicate *DBNPredicate = [NSPredicate predicateWithFormat:@"dbn = %@",DBN];

    return [SATsArray filteredArrayUsingPredicate:DBNPredicate].firstObject;
}
@end
