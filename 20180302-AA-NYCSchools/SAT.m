//
//  SAT.m
//  20180302-AA-NYCSchools
//
//  Created by Argueta, Adan (CHICO-C) on 3/2/18.
//  Copyright © 2018 Argueta, Adan (CHICO-C). All rights reserved.
//

#import "SAT.h"

@implementation SAT

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             NSStringFromSelector(@selector(dbn)) : @"dbn",
             NSStringFromSelector(@selector(name)) : @"school_name",
             NSStringFromSelector(@selector(takersCount)) : @"num_of_sat_test_takers",
             NSStringFromSelector(@selector(readingScore)) : @"sat_critical_reading_avg_score",
             NSStringFromSelector(@selector(writingScore)) : @"sat_writing_avg_score",
             NSStringFromSelector(@selector(mathScore)) : @"sat_math_avg_score"
             };
}
@end
