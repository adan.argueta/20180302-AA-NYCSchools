//
//  School.h
//  20180302-AA-NYCSchools
//
//  Created by Argueta, Adan (CHICO-C) on 3/2/18.
//  Copyright © 2018 Argueta, Adan (CHICO-C). All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Mantle.h"

@interface School : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong) NSString *dbn;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *overview;

@end
